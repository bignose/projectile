projectile (2.0.0-1) unstable; urgency=medium

  * Update to new upstream version 2.0.0.
    - Drop elpa-dash, elpa-noflet build-deps
    - Add elpa-buttercup build-dep
    - Drop d/elpa-test
      No longer needed thanks to upstream test suite changes.
  * Patch projectile--directory-p test for the Debian package build.
    Stop it from assuming that the user's home directory exists.
  * Patch the test init code & macros in projectile-test.el.
    - Stop it from trying to load projectile.el
      dh_elpa_test handles doing this.
    - Don't choose a sandbox location using `locate-library'.
      Creating the sandbox in /usr/share will fail, breaking the autopkgtest.
    - Load subr-x before running any tests.
      In this release, loading subr-x is wrapped in an eval-when-compile.
      Buttercup's macros don't seem to be able to cope with this.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 11 Jan 2019 20:18:32 -0700

projectile (1.0.0-2) unstable; urgency=medium

  [ Dmitry Shachnev ]
  * Stop overriding mkdocs theme, search works fine now.
  * Use dh_mkdocs instead of dh_linktree.
  * Prevent search_index.json from being compressed.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 19 Dec 2018 12:21:15 +0000

projectile (1.0.0-1) unstable; urgency=medium

  * Update to new upstream version 1.0.0.
    - Refresh patches
    - Update debian/elpa-test.
  * Drop Debian changes to .gitignore.
    Mostly outdated and not especially useful, and prone to merge
    conflicts with new upstream releases.
  * Update debian/projectile-doc.linktrees for changes in mkdocs.
  * Patch test-helper.el so tests will run under autopkgtest too.
  * Install CHANGELOG.md as NEWS.gz.
    - New build-dep on dh-exec
    - Add override_dh_installchangelogs stanza.
      Avoids installing the changelog twice, as both NEWS.gz and changelog.gz.
  * Update Maintainer field for team rename.
  * Point Vcs-* at salsa.
  * Drop Built-Using.
    Used incorrectly in this package; see Debian Policy.
  * Enhance emacs25, not emacs24.
  * Drop --parallel from d/rules.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 11 Aug 2018 09:42:15 +0100

projectile (0.14.0-2) unstable; urgency=medium

  * Update dependency dash-el => elpa-dash (Closes: #865915).
    Thanks to Mykola Nikishov for reporting the problem.
    - Bump dh-elpa build dependency to (>= 1.7).
      Ensures that elpa-dash is included in ${elpa:Depends}.
  * Add build-dependency on mkdocs-bootstrap (Closes: #884772).
    Thanks Antonio Terceiro for reporting the bug and indicating the fix.
  * Correct debhelper build-dep bound to match compat.
  * Switch from d/rules DH_ELPA_TEST_ERT_HELPER to entry in d/elpa-test
    config file (see dh_elpa_test(1)).
  * Drop d/source/local-options for dgit.
  * Declare compliance with Debian Policy 4.1.2.
  * Drop unused Lintian overrides:
    - package-needs-versioned-debhelper-build-depends
    - package-uses-experimental-debhelper-compat-version
    - privacy-breach-generic *html5shiv.js*
    - privacy-breach-generic *respond.min.js*

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 22 Dec 2017 11:40:19 +0000

projectile (0.14.0-1) unstable; urgency=medium

  * New upstream release.
    - elpa-persp-projectile & elpa-helm-projectile binary packages dropped.
      They now have their own source packages.
  * New projectile-doc binary package installing HTML manual.
    - elpa-projectile Suggests: projectile-doc
    - Add dh sequencer argument: --with linktree
    - New build dependency on dh-linktree
    - Add doc-base registration
    - Add d/clean
    - Add projectile-doc.lintian-overrides
  * Run test suite with dh_elpa_test.
    - Bump debhelper compat to 10.
    - Add source Lintian overrides:
      - package-needs-versioned-debhelper-build-depends
      - package-uses-experimental-debhelper-compat-version
  * Quilt patches:
    - Append to 0001-patch-README.patch to remove screenshot.
      Screenshots now installed by projectile-doc.
    - Drop 0003-fix-cleanup-known-projects-test.patch
      Merged upstream.
    - Rename 0004-debianise-exuberant-ctags-dep.patch
          to 0003-debianise-exuberant-ctags-dep.patch
    - Add 0004-drop-external-gratipay-image.patch
    - Add 0005-configure-mkdocs-for-Debian.patch
    - Refresh patches
  * d/copyright updates:
    - bump copyright years and add upstream author's e-mail address
    - remove paragraph for persp-projectile.el
  * Run wrap-and-sort -abst
  * Bump standards version to 3.9.8 (no changes required).

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 24 Jul 2016 19:39:18 -0700

projectile (0.13.0-1) unstable; urgency=medium

  * Initial release. (Closes: #807839)

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 21 Feb 2016 13:01:59 -0700
